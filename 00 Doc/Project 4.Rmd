---
title: "Project 4"
author: "John Mitchell, Matt Strasburg, John Dominguez"
date: "October 26, 2016"
output:
  html_document:
    toc: yes
  html_notebook:
    toc: yes
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Session Info
```{r }
sessionInfo()
```

##ETL Operations
For this project we used a csv containing different countries CO2 emissions by year. Using the following ETL file, we were able to clean up the csv and use the created print statement to create the table in SQL Developer. Using the outputed csv, we were able to add the data to the table so we could use it in our Shiny application.

ETL Operations:
```{r}
  source("../01 Data/R_ETL.R", echo = TRUE)
```

##Shiny App
Graph 1 charts CO2 emissions for United States, Canada, France, and Germany for every year that CO2 emissions have been tracked.
Graph 2 charts CO2 emissions yearly in the USA since 1990.
Graph 3 charts CO2 emissions for United States, Canada, France, and Germany for the year 2010.
```{r}
  source("../02Shiny/server.R", echo = FALSE)
```
