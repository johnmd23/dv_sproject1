#ui.R

require(shiny)
require(shinydashboard)
require(leaflet)

dashboardPage(
  
  dashboardHeader(title = 'Project'),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem("plot1", tabName = "plot1", icon = icon("dashboard")),
      menuItem("plot2", tabName = "plot2", icon = icon("th")),
      menuItem("plot3", tabName = "plot3", icon = icon("th")))
    ),
  
  dashboardBody(
    tabItems(
      
      #first tab
      tabItem(tabName = "plot1",
        h2("Emissions For Different Nations"),
        plotOutput("Plot1")
      ),

      #second tab
      tabItem(tabName = "plot2",
        plotOutput("Plot2")
      ),
      
      #third tab
      tabItem(tabName = "plot3",
        h2("Emissions for year 2010"),
        plotOutput("Plot3")
      )
    )
  )
)
